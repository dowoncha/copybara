import sys
from pathlib import Path
import os
import glob
import re
import mechanicalsoup
from collections import Counter
import csv
import nltk

from flask import Flask, request, jsonify, send_from_directory, render_template
from flask_cors import CORS


nltk_data_installed = False

nltk_data = 'averaged_perceptron_tagger'
try:
    nltk.download(nltk_data)
    nltk_data_installed = True
except:
    pass

def gen_clean_filename(filename):
    return "".join([c for c in filename if re.match(r'\w', c)])

def extract_link_target(target):
    # Filter out unrelated links
    if (target.startswith('/url?') and not
            target.startswith("url?q=https://webcache.googleusercontent.com")):
        target = re.sub(r"^/url\?q=([^&]*)&.*", r"\1", target)

        return target

def scrape_search_result_page(browser, filename, target, outDir):
    try:
        browser.open(target)
    except:
        return

    pageSoup = browser.get_current_page()

    if (not pageSoup):
        return

    if (pageSoup.head):
        pageSoup.head.decompose()

    # Delete all scripts
    # [s.extract() for s in pageSoup('script')]
    #if (pageSoup.script):
    #     pageSoup.script.decompose()

    # if (pageSoup.style):
    #    pageSoup.decompose()

    # Delete all css attributes
    for tag in pageSoup:
        try:
            for attribute in ['class', 'id', 'name', 'style']:
                del tag[attribute]
        except:
            pass

    # Generate a clean filename by comibining the link title
    cleanedFilename = gen_clean_filename(filename)
    cleanedFilename += ".txt"

    pageText = pageSoup.get_text()

    # cleanedText = pageSoup.stripped_string
    # Strip all new lines
    cleanedText = pageText.replace('\n', ' ')

    pagesFilepath = os.path.join(outDir, "pages")
    if not os.path.exists(pagesFilepath):
        os.makedirs(pagesFilepath)

    # print(soup.get_text())
    linkFilepath = os.path.join(outDir, "pages", cleanedFilename)
    with open(linkFilepath, 'w+') as f:
        f.write(cleanedText)

    print("Finished extracting to ", linkFilepath)

def gen_word_frequency(searchTerm):
    outDir = gen_clean_filename(searchTerm)
    print("Output results to: ", outDir)

    if not os.path.exists(outDir):
        print("Directory " + outDir + " created.")
        os.makedirs(outDir)
    else:
        print("Directory " + outDir + "found.")

    # Navigate to search engine and search
    browser = mechanicalsoup.StatefulBrowser()

    searchEngine = "https://www.google.com"

    browser.open(searchEngine)

    formSelector = 'form[action="/search"]'

    browser.select_form(formSelector)
    # browser["q"] = "".join("allintext: ", '"', searchTerm, '"')
    browser["q"] = 'allintext: "' + searchTerm + '"'

    # For google, btnG is used for bots, btnK for users
    browser.submit_selected(btnName="btnG")

    # scrape_search_result_page(browser)
    # Grab all links
    linkTags = []

    for link in browser.links():
        linkTags.append(link)

    print("Gathering all link tag hrefs in file")
    with open(os.path.join(outDir, "link-tags.txt"), "w+") as f:
        for link in linkTags:
            f.write(link.attrs['href'] + '\n')

    print("Extracting links from href")
    for linkTag in linkTags:
        name = linkTag.text
        target = extract_link_target(linkTag.attrs['href'])
        # print("Extracted target: %s from href: %s" % (target, linkTag.attrs['href']))

        if (name == "Cached"):
            continue

        if (not target):
            continue

        if (not (target.startswith("http://") or target.startswith("https://"))):
            continue

        print("Accepted: ", target)

        scrape_search_result_page(browser, name, target, outDir)

    # 3. Generate word frequency
    # Read all txt files
    # combine all into a single string
    # create word frequency list
    # output to results file
    # read_files = glob.glob(os.path.join(outDir, "pages", '*.txt'))
    readFiles = Path(os.path.join(outDir, "pages")).glob('**/*.txt')

    print("Combining all txt files into a single file")
    resultsFilepath = os.path.join(outDir, "results.txt")

    with open(resultsFilepath, "wb") as outfile:
        for f in readFiles:
            with open(f, "rb") as infile:
                outfile.write(infile.read().lower())

    # Read results file
    with open(resultsFilepath, "r") as f:
        data = f.read()

        print("Opening total word results: " + str(len(data))) # remove all non characters data = data.replace("[^a-zA-Z]", "")
        print("Removing all non word characters: " + str(len(data)))
        data.lower()

        print("Converting all characters to lowercase")

        # Remove all non words

        # Split string into array of words
        words = data.split(' ')

        # Remove all non words
        words = [word for word in words if word.isalpha()]
        print("Removing all words with non character values: ", str(len(words)))

        if nltk_data_installed:
            print("Filtering out all non nouns")
            # Tag words
            wordTags = nltk.pos_tag(words)

            # Remove all non words
            # words = [word for tag in wordTags if tag[1].startswith('N')]
            filteredWordTags = filter(lambda wordTag: wordTag[1].startswith('N') or wordTag[1] == 'FW', wordTags)
            words = list(map(lambda wordTag: wordTag[0], filteredWordTags))
        else:
            print("nltk data not installed. Skipping nltk filter")

        print("Filtering out all non nouns: " + str(len(words)))

        frequency = Counter(words).most_common()

        return frequency

app = Flask(__name__, static_folder="ui/build")
CORS(app)

@app.route("/api/search", methods=["GET"])
def search():
        searchTerm = request.args.get("q")

        word_frequency = gen_word_frequency(searchTerm)

        return jsonify(word_frequency)
