# Copybara
Copybara is a search term keyword scraper. It searches websites that match
the given search term and generates a csv file with a word frequency chart of
most used keywords.


## Installation
Requires python3

Install virtualenv

```pip install virtualenv```

Create a new venv

```virtualenv venv -p python3```

Load venv

```source venv/bin/activate```

Install dependencies

```pip install -r requirements.txt```

## Usage
```python copybara.py [SEARCHTERM]```
```python copybara.py "blockchain consultancy"```

Copybara will search the search engine with term.
It'll gather all of the result links and load their html.
The html is cleaned of all scripts and style tags and combined into a single file.
Then read the file and generate csv of word frequencies.
The system uses NLTK to filter and keep only Nouns and Foreign Words, other word
tags may be added.




