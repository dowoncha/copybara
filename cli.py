import sys
import os
import csv
from copybara import gen_word_frequency, gen_clean_filename

def write_word_frequency_file(frequency, outdir):
    print("Writing to word-frequency.csv")
    wordFrequencyFilepath = os.path.join(outdir, "word-frequency.csv")
    with open(wordFrequencyFilepath, "w") as outfile:
        frequencyWriter = csv.writer(outfile, delimiter=' ')

        frequencyWriter.writerow(["word", "frequency"])

        for word in frequency:
            frequencyWriter.writerow([word[0], word[1]])

def main():
    if (len(sys.argv) < 2):
        print("python copybara.py [SEARCHTERM]")
        return

    searchTerm = sys.argv[1]

    frequency = gen_word_frequency(searchTerm)
    outdir = gen_clean_filename(searchTerm)

    write_word_frequency_file(frequency, outdir)

if __name__ == "__main__":
  main()

